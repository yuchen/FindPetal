import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch, Polygon
"""
Have a look of this page for more details about how it works:
http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html
"""
def drawMatches(img_q, kp_q, img_t, kp_t, matches, **draw_params):
    """Draw the matching points, connecting lines between them, and the window.

    An full matplotlib alternative of __rayryeng__'s implementation of cv2.drawMatches as OpenCV 2.4.9
    does not have this function available but it's supported in OpenCV 3.0.0

    This function takes in two images with their associated
    keypoints, as well as a list of DMatch data structure (matches)
    that contains which keypoints matched in which images.

    An image will be produced where a montage is shown with
    the first image followed by the second image beside it.

    Keypoints are delineated with circles, while lines are connected
    between matching keypoints.
    
    Parameters
    ----------
    img_q : image
        Query image
    kp_q : list
        Detected list of keypoints of the query image through any of the OpenCV keypoint detection algorithms
    img_t : image
        Train image
    kp_t : list
        Detected list of keypoints of the train image through any of the OpenCV keypoint detection algorithms
    matches : list
        A list of matches of corresponding keypoints through any OpenCV keypoint matching algorithm
    **draw_params : dict
        Extra parameters will be put into the __CollectionPath__
    
    Returns
    -------
    Figure, (Axis, Axis)
        The figure, an axis for the query image and another for the train image.
    """
    fig, (query, train) = plt.subplots(1, 2, gridspec_kw = {'width_ratios':[img_q.shape[1], img_t.shape[1]]})
    query.set_title("query")
    train.set_title("train")
    train.set_zorder(-1)
    query.imshow(img_q, cmap = "gray", aspect = "equal")
    train.imshow(img_t, cmap = "gray", aspect = "equal")
    for mat, mask in zip(matches, draw_params.get("matchesMask")):
        # Get the matching keypoints for each of the images
        img_q_idx = mat.queryIdx
        img_t_idx = mat.trainIdx
        # x - columns
        # y - rows
        (x_q,y_q) = kp_q[img_q_idx].pt
        (x_t,y_t) = kp_t[img_t_idx].pt
        # Draw a small circle at both co-ordinates
        query.scatter(x_q, y_q, s = 20, color = "red", marker = "o", facecolors = "none")
        train.scatter(x_t, y_t, s = 20, color = "red", marker = "o", facecolors = "none")
        # Draw a line in between the two points
        l = ConnectionPatch(xyA = (x_q,y_q), coordsA = "data", axesA = query,
                            xyB = (x_t,y_t), coordsB = "data", axesB = train,
                            color = draw_params.get("matchColor")[bool(mask)], lw = 1)
        query.add_artist(l)
    return fig, (query, train)

class PerspectiveTransform(object):
    MIN_MATCH_COUNT = 10
    def __init__(self, query, train):
        self.query = cv2.imread(query, 0) # queryImage
        self.train = cv2.imread(train, 0) # trainImage
        self._feature_matching()
        self._find_homography()

    def _feature_matching(self, query = None, train = None):
        if query is None:
            query = self.query
        if train is None:
            train = self.train
        # Initiate SIFT detector
        if cv2.__version__.startswith("3"):
            sift = cv2.xfeatures2d.SIFT_create()
        else:
            sift = cv2.SIFT()

        # find the keypoints and descriptors with SIFT
        self._kp1, des1 = sift.detectAndCompute(query, None)
        self._kp2, des2 = sift.detectAndCompute(train, None)

        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks = 50)

        flann = cv2.FlannBasedMatcher(index_params, search_params)

        # store all the good matches as per Lowe's ratio test.
        self._good = [m for m,n in flann.knnMatch(des1, des2, k = 2) if m.distance < 0.7*n.distance]

        if len(self._good) <= self.MIN_MATCH_COUNT:
            raise RuntimeError("Not enough matches are found - %d/%d" % (len(self._good), self.MIN_MATCH_COUNT))

    def _find_homography(self):
        src_pts = np.float32([self._kp1[m.queryIdx].pt for m in self._good]).reshape(-1, 1, 2)
        dst_pts = np.float32([self._kp2[m.trainIdx].pt for m in self._good]).reshape(-1, 1, 2)
        self._H, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        self._matchesMask = mask.ravel().tolist()

    def view_finder(self, pts = None, homography = None, inverted = False, ret_dst = False, **draw_params):
        if homography is None:
            homography = self._H
        M = homography if not inverted else np.linalg.inv(homography)
        h,w = self.query.shape[:2] if not inverted else self.train.shape[:2]
        if pts is None:
            pts = np.float32([[0,0], [0, h-1], [w-1, h-1], [w-1, 0]])
        if pts.shape[-1] == 2 and pts.ndim == 2:
            dst = cv2.perspectiveTransform(pts.reshape(-1, 1, 2), M).reshape(-1, 2)
        else:
            dst = cv2.warpPerspective(pts, M, self.query.shape[:2][::-1] if inverted else self.train.shape[:2][::-1])
        if ret_dst:
            return dst
        draw_params = dict(lw = 2, 
                           edgecolor = "blue", 
                           facecolor = "none",
                           **draw_params)
        return Polygon(dst, closed = True, **draw_params)

    def draw_matches(self, **draw_params):
        draw_params = dict(dict(matchColor = ("yellow", "lime"), # draw outlying/inlying matches in yellow/green color respectively
                                matchesMask = self._matchesMask),# draw only inliers
                           **draw_params) 
        fig, (query, train) = drawMatches(self.query, self._kp1, self.train, self._kp2, self._good, **draw_params)
        view = self.view_finder()
        train.add_patch(view)
        query.axis("off")
        train.axis("off")
        plt.tight_layout()
        plt.autoscale(enable=False)
        return fig, (query, train)
    @classmethod
    def get_perspective_transform(self, src, dst):
    	return cv2.getPerspectiveTransform(src, dst)

def test():
    pt = PerspectiveTransform('161109_215516.png', '161109_222838.png')
    fig, (query, train) = pt.draw_matches()
    fig.show()
if __name__ == "__main__":
    test()