import find_petal as FP
import poly_editor as PE
from matplotlib.patches import Polygon
import matplotlib.pyplot as plt
import numpy as np
from functools import partial

def ref2view(perspective_transform, img_ref, plane_ref, plane_view):
	M = FP.PerspectiveTransform.get_perspective_transform(plane_ref, plane_view)
	img_view = perspective_transform.view_finder(img_ref, M, ret_dst = True)
	return img_view

plane_ref = np.float32([(66,0), (1490,350), (1490,1222), (50,1538)])
plane_view = np.float32([(754,28), (2186,374), (2186,1244),(748,1552)])
img_ref = plt.imread("template.png")

pt = FP.PerspectiveTransform('161109_215516.png', '161109_222838.png')
fig, (query, train) = pt.draw_matches()

tri = Polygon(plane_view, edgecolor = "b", facecolor = "none", animated = True)
train.add_patch(tri)
ref2view = partial(ref2view, pt, img_ref, plane_ref)
pi = PE.PolygonInteractor(train, tri, ref2view(plane_view), ref2view)
pi.set_mirror(pt.view_finder, query, inverted = True, ret_dst = True)
print '\033[1;32m' + 'Click and drag a point to move it' + '\033[0m'
plt.show()